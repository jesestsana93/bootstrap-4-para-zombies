Curso: Bootstrap 4 para zombies - !De novato a sensei! por Erick Mines
Duración: 7:16 hrs

Accesibilidad:
Probador de lector de pantallas: https://support.freedomscientific.com/Downloads/JAWS

Notas:
En links a los atributos a poner que ofrece Bootstrap 4 son:
role="button"
aria-haspopup="true" //indica la lector de pantalla que hay un elemento desplegable
aria-expandend="false" //dice al lector de pantalla el estado por defecto del elemento desplegable 
id="menu-desplegable" //identificamos el boton

En divs:
aria-labelledby="menu-desplegable" //crear una asociacion entre el elemento que se desplega y el botton

Efecto de la animación de las letras: https://mattboldt.com/demos/typed-js/